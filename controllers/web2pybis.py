# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# -------------------------------------------------------------------------
# This is a sample controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# -------------------------------------------------------------------------


def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()

##############################
# custom definitions
# anazeer 12.01.2017
import urllib
import hashlib
import sys
#sys.path.append("/home/galaxy/tool_libs/lib64/python2.6/site-packages")
#sys.path.append("/home/galaxy/.local/lib/python2.6/site-packages")
import jsonrpclib, json
import requests, os, subprocess
import tempfile, shutil
import re
import time
from bioblend import galaxy
import warnings
warnings.filterwarnings("ignore")
#import rlcompleter, readline
#readline.parse_and_bind('tab:complete')



#openbisurl='https://localhost:8443/openbis'
openbisurl='https://openBIS:8443/openbis'
galaxyurl='http://localhost:8080'
openbisapijar='/dirshare/openbis/openbisapi/IbeOpenbis_stable.jar'
#openbiskeystore=' -k /home/galaxy/ext_tools/openbisapi/openBIS.keystore'
openbiskeystore=''
#javapath='/home/openbis/downloads/openbis/jre1.7.0_79/bin/java'
javapath='/usr/bin/java'

def getCredentials():
    '''
    '''
    gurl = "%s/%s" % (request.url,request.env.query_string)
    gurl = urllib.unquote(gurl).decode('utf8')
    gemail = gkey = ouser = opass = entryid = "None"
    for tmptxt in gurl.split("&"):
        if "CurrentUser=" in tmptxt:
            gemail = tmptxt.split("=")[1]
    if gemail != "None":
        s = db(db.galaxy_openbis_connector1)
        rows = s.select()
        for row in rows:
            if row.guseremail == gemail:
                entryid = row.id
                gkey = row.gapikey
                ouser = row.ousername
                opass = row.opassphrase
    return gemail, gkey, ouser, opass, entryid

def initiate():
    #gemail, gkey, ouser, opass, entryid = getCredentials()
    form = SQLFORM.factory(Field('ousername',
                      type='string',
                      #label=T('Openbis User Name'), default="admin"),
                      label=T('Openbis User Name')),
                Field('opassphrase',
                      #type='string',
                      type='password',
                      label=T('Openbis Password')),
                Field('gapikey',
                      type='password',
                      label=T('Galaxy API Key')),
                Field('state',
                      type='string',
                      requires=IS_IN_SET(['Registered User','Update Fields','New User']),
                      widget=SQLFORM.widgets.radio.widget,
                      default='Registered User'))
    #submit = form.element("input",_type="submit")
    #submit["_onclick"] = "return confirm('Are you sure?');"
    if form.process().accepted:
        tmpvars = form.vars
        registrationcheck = RegistrationCheck(tmpvars.gapikey,tmpvars.ousername,tmpvars.opassphrase,tmpvars.state)
        session.gkey = form.vars.gapikey
        session.ouser = form.vars.ousername
        session.opass = form.vars.opassphrase
        #redirect(URL('user_todo'))
        redirect(URL('user_select'))
    return dict(form=form)

def RegistrationCheck(gapikey,ousername,opassphrase,state):
    if not gapikey or not ousername or not opassphrase:
        session.flash=T("Fill all required fields")
        redirect(URL('oops'))
    gapikey = str(hashlib.md5(gapikey).hexdigest())
    opassphrase = str(hashlib.md5(opassphrase).hexdigest())
    gemail, gkey, ouser, opass, entryid = getCredentials()
    session.gemail = gemail
    registrationcheck = 'Failure'
    if gemail == "None":
        session.flash=T("This service is specific for use cases inside galaxy!")
        redirect(URL('oops'))
    else:
        if state == "Registered User":
            if entryid == "None":
                session.flash=T("User doesn't exists!")
                redirect(URL('oops'))
            if gapikey == gkey and ousername == ouser and opassphrase == opass:
                registrationcheck = 'Success3'
            else:
                session.flash=T("Wrong Username/password/apikey")
                redirect(URL('oops'))
        if state == "New User":
            if entryid != "None":
                session.flash=T("User already exists!")
                redirect(URL('oops'))
            db.galaxy_openbis_connector1.insert(guseremail=gemail, gapikey=gapikey,ousername=ousername, opassphrase=opassphrase)
            registrationcheck = 'Success1'
        elif state == "Update Fields":
            if entryid == "None":
                session.flash=T("User doesn't exists!")
                redirect(URL('oops'))
            rows = db(db.galaxy_openbis_connector1).select()
            db(db.galaxy_openbis_connector1.id == entryid).update(guseremail=gemail, gapikey=gapikey,ousername=ousername, opassphrase=opassphrase)
            registrationcheck = 'Success2'
    return registrationcheck

def oops():
    return dict()

def pvar():
    return dict(tmp=request.vars.tmp)

def user_todo():
    galaxySession(galaxyurl,session.gkey)
    return dict(gworkflowids=session.gworkflowids)

def user_select():
    galaxySession(galaxyurl,session.gkey)
    srvr = jsonrpclib.Server(openbisurl + '/openbis/rmi-general-information-v1.json')
    session.st = srvr.tryToAuthenticateForAllServices(session.ouser, session.opass)
    projects = srvr.listProjects(session.st)
    session.projects = projects
    spacecodes, projectcodes = ProjectLists(projects)
    session.spacecodes = spacecodes
    session.projectcodes = projectcodes
    experimenttypes = srvr.listExperimentTypes(session.st)
    sampletypes = srvr.listSampleTypes(session.st)
    propertytypelists = srvr.listPropertyTypes(session.st,1)
    controlledvocabs = srvr.listVocabularies(session.st)
    session.sampletypes = sampletypes
    session.propertytypelists = propertytypelists
    session.controlledvocabs = controlledvocabs
    experimenttypelists = []
    for e in experimenttypes:
        experimenttypelists.append(e['code'])
    session.experimenttypelists = experimenttypelists
    session.sampletypelists = []
    for s in sampletypes:
        session.sampletypelists.append(s['code'])
    return dict(gworkflowids=session.gworkflowids)

def workflow_getvars():
    # galaxy workflows
    session.final_gworkflowid = request.vars.galaxy_workflow
    WorkflowInputs(session.final_gworkflowid)
    srvr = jsonrpclib.Server(openbisurl + '/openbis/rmi-general-information-v1.json')
    session.st = srvr.tryToAuthenticateForAllServices(session.ouser, session.opass)
    projects = srvr.listProjects(session.st) # get space and projects code
    session.projects = projects
    spacecodes, projectcodes = ProjectLists(projects)
    session.spacecodes = spacecodes
    session.projectcodes = projectcodes
    experimenttypes = srvr.listExperimentTypes(session.st)
    sampletypes = srvr.listSampleTypes(session.st)
    propertytypelists = srvr.listPropertyTypes(session.st,1)
    controlledvocabs = srvr.listVocabularies(session.st)
    session.sampletypes = sampletypes
    session.propertytypelists = propertytypelists
    session.controlledvocabs = controlledvocabs
    experimenttypelists = []
    for e in experimenttypes:
        experimenttypelists.append(e['code'])
    session.experimenttypelists = experimenttypelists
    session.sampletypelists = []
    for s in sampletypes:
        session.sampletypelists.append(s['code'])
    return dict(spacecodes=spacecodes, projectcodes=projectcodes,experimenttypelists=experimenttypelists )

def WorkflowSamples():
    #result = "<h3>Galaxy Workflow: " + str(session.gwinputs) + "</h3>"
    #result += "<h3>Galaxy Workflow: " + str(session.final_gworkflowid) + "</h3>"
    result = ""
    tmpint = 0
    for gwinput in session.gwinputs:
        tmpi = str(tmpint)
        result += "<label id='source" + tmpi + "' for='source_" + tmpi + " + '>" + str(gwinput[1]) + ":</label>"
        result += "<select name='source_" + tmpi + "' id='source_" + tmpi + "' onchange=\"ajax('SelectSampleSource', ['source_" + tmpi + "'] ,'sample_" + tmpi + "');\">"
        result += "<option disabled selected value> -- select your source -- </option>"
        result += "<option value='galaxy," + str(gwinput[0]) + "," + str(gwinput[1]) + "," + str(gwinput[2]) + "'>From Galaxy</option>"
        result += "<option value='openbis," + str(gwinput[0]) + "," + str(gwinput[1]) + "," + str(gwinput[2]) + "'>From OpenBIS</option></select>"
        result += "<span id='sample_" + tmpi + "'></span><br>"
        tmpint += 1
        result += "<hr>"
    return XML(result)


def SelectSampleSource():
    for tmpint in range(0, len(session.gwinputs)):
        exec('session.gwinput = request.vars.source_' + str(tmpint))
        if session.gwinput:
            break
    tmpi = str(tmpint)
    session.tmpi = tmpi
    sampleinfos = session.gwinput.split(",")
    ssource = str(sampleinfos[0])
    session.gwinputid = str(sampleinfos[1])
    session.gwinputanno = str(sampleinfos[3])
    result = ""
    if ssource == 'galaxy':
        result = "<label id='ghname" + tmpi + "' for='ghname_" + tmpi + "'>Galaxy History: </label>"
        result += "<select name='ghname_" + tmpi + "' id='ghname_" + tmpi + "' onchange=\"ajax('SamplesFromGalaxy', ['ghname_" + tmpi + "'],'selected_samples_" + tmpi + "');\">"
        result += "<option disabled selected value> -- select history -- </option>" 
        for h in session.ghistoryids:
            result += "<option value='" + str(h[1]) + "'>" + str(h[0]) + "</option>"
        result += "</select><br>"
    if ssource == 'openbis':
        result = "<br><label id='space_name" + tmpi + "' for='space_name_" + tmpi + "'>Openbis Space: </label>"
        result += "<select name='space_name_" + tmpi + "' multiple onchange=\"ajax('ProjectName',['space_name_" + tmpi + "'], 'selected_samples_" + tmpi + "');\">"
        result += "<option disabled selected value> -- select space -- </option>"
        for spacecode in session.spacecodes:
            result += "<option value='" + spacecode + "'>" + spacecode + "</option>"
        result += "</select>"
    result += "<span id='selected_samples_" + tmpi + "'></span><br>"
    return XML(result)

def workflow_history():
    final_gworkflowid = session.final_gworkflowid
    final_gworkflowname = session.gi.workflows.show_workflow(final_gworkflowid)['name']
    gwinputs = session.gwinputs
    maxinputs = 0
    for tmpint in range(0, len(gwinputs)):
        exec('selected_samples = request.vars.selected_sample_' + str(tmpint))
        #try: selected_samples = selected_samples.split("#####")
        #except AttributeError: pass
        try:
            tmp_selected_samples = selected_samples.split("#####")
            if tmp_selected_samples[0].split(',')[0] == "openbis":
                selected_samples = selected_samples.split(',')[1:]
            else:
                selected_samples = tmp_selected_samples
        except AttributeError: pass
        if len(selected_samples) > maxinputs:
            maxinputs = len(selected_samples)
    session.final_dataset_dicts = [None] * maxinputs
    for tmpint in range(0, len(gwinputs)):
        exec('selected_samples = request.vars.selected_sample_' + str(tmpint))
        #try: selected_samples = selected_samples.split("#####")
        #except AttributeError: pass
        try:
            tmp_selected_samples = selected_samples.split("#####")
            if tmp_selected_samples[0].split(',')[0] == "openbis":
                selected_samples = selected_samples.split(',')[1:]
                #galaxysamplesource = 'ldda' # ldda not workfing
                galaxysamplesource = 'ld'
            else:
                selected_samples = tmp_selected_samples
                galaxysamplesource = 'hda'
        except AttributeError: pass
        if 'None' in selected_samples:
            session.flash=T('Please select a valid dataset!!!')
            redirect(URL('oops'))
        for tmpvar in range(0, len(session.final_dataset_dicts)):
            if len(selected_samples) == 1:
                selected_sample = selected_samples[0]
                selected_sample = selected_sample.split("#$#")
            elif len(selected_samples) == maxinputs:
                selected_sample = selected_samples[tmpvar]
                selected_sample = selected_sample.split("#$#")
            else:
                session.flash=T('Uneven number of datasets selected!!!!')
                redirect(URL('oops'))
            if tmpint == 0:
                session.final_dataset_dicts[tmpvar] = {str(gwinputs[tmpint][0]) : {'id' : str(selected_sample[0]), 'src': galaxysamplesource , 'name' : str(selected_sample[1])}}
            else:
                session.final_dataset_dicts[tmpvar].update({str(gwinputs[tmpint][0]) : {'id' : str(selected_sample[0]), 'src': galaxysamplesource , 'name' : str(selected_sample[1])}})
    return dict(workflow_name=final_gworkflowname, tmp=session.final_dataset_dicts)

def workflow_run():
    for final_dataset_dict in session.final_dataset_dicts:
        dataset_names = ""
        tmpsep = ""
        for tmpf in final_dataset_dict:
            dataset_names += tmpsep + str(final_dataset_dict[tmpf]['name'])
            tmpsep = ", "
            del(final_dataset_dict[tmpf]['name'])
        final_history_name = str(request.vars.new_history) + ' on ' + dataset_names
        try:
            new_workflow = session.gi.workflows.run_workflow(session.final_gworkflowid, dataset_map = final_dataset_dict, history_name = final_history_name, import_inputs_to_history=True)
        except:
            pass
    redirect(URL('finito'))
    return dict()

def finito():
    return dict()
 
def SamplesFromGalaxy():
    tmpi = session.tmpi
    gwinputanno = str(session.gwinputanno).split("+")
    gwinputanno = [x.upper().lower() for x in gwinputanno]
    exec('ghid = request.vars.ghname_' + tmpi)
    result = "<label id='selected_sample" + tmpi + "' for='selected_sample_" + tmpi + "'>Galaxy Dataset:</label>"
    result += "<select name='selected_sample_" + tmpi + "' id='selected_sample_" + tmpi + "' multiple>"
    result += "<option disabled selected value> -- select dataset -- </option>"
    history_dict = session.gi.histories.show_history(ghid)
    for gdid in history_dict['state_ids']['ok']:
        dataset_dict = session.gi.histories.show_dataset(ghid,gdid)
        if 'None'.upper().lower() in gwinputanno or str(dataset_dict['file_ext']).upper().lower() in gwinputanno:
            result += "<option value='" + str(dataset_dict['id']) + "#$#" + str(dataset_dict['name']) + "'>" + str(dataset_dict['name']) + "</option>"
            #result += "<option value='" + str(dataset_dict['id']) + "'>" + str(dataset_dict['name']) + "</option>"
    result += "</select>"
    result += "<sub>      Caution: Takes time to reload datasets for bigger histories!!!</sub>"
    return XML(result) 

#def SamplesFromOpenBIS():
#    tmpi = session.tmpi
#    srvr = jsonrpclib.Server(openbisurl + '/openbis/rmi-general-information-v1.json')
#    osampletypes = srvr.listSampleTypes(session.st)
#    result = "<label id='selected_sample" + tmpi + "' for='selected_sample_" + tmpi + "'>Galaxy Dataset:</label>"
#    return dict(tmp=osampletypes)

def tmp():
    return dict()

def galaxySession(galaxyurl, gkey):
    session.gi = galaxy.GalaxyInstance(url=galaxyurl, key=gkey)
    gi = session.gi
    ghistories = gi.histories.get_histories()
    gworkflows = gi.workflows.get_workflows()
    session.gworkflowids = []
    session.ghistoryids = []
    for gworkflow in gworkflows:
        session.gworkflowids.append([gworkflow['name'],gworkflow['id']])
    for ghistory in ghistories:
        session.ghistoryids.append([ghistory['name'],ghistory['id']])
    return

def ProjectLists(projects):
    svar = []
    pvar = []
    for p in projects:
        svar.append(p['spaceCode'])
        pvar.append('/' + p['spaceCode'] + '/' + p['code'])
    return sorted(list(set(svar))), sorted(list(set(pvar)))

def ProjectName():
    tmpi = session.tmpi
    exec('spacecodes = request.vars.space_name_' + tmpi)
    projectcodes = session.projectcodes
    if not spacecodes:
        spacecodes = str(spacecodes).split()
    try: spacecodes = spacecodes.split()
    except AttributeError: pass
    result = "<label id='project_name" + tmpi + "' for='project_name_" + tmpi + "'>Project: </label>"
    result += "<select name='project_name_" + tmpi + "' id='project_name_" + tmpi + "' multiple onchange=\"ajax('ExperimentType', [], 'experiment_type_" + tmpi + "');\">"
    result += "<option disabled selected value> -- select project -- </option>"
    for projectcode in projectcodes:
        for spacecode in spacecodes:
            if projectcode.startswith('/' + spacecode + '/'):
                result += "<option value='" + str(projectcode) + "'>" + str(projectcode) + "</option>"
    result += "</select>"
    result += "<span id='experiment_type_" + tmpi + "'></span>"
    return XML(result)

def ExperimentType():
    tmpi = session.tmpi
    result = "<label id='experiment_type" + tmpi + "' for='experiment_type_" + tmpi + "'>Experiment Type: </label>"
    result += "<select name='experiment_type_" + tmpi + "' id='experiment_type_" + tmpi + "' multiple onchange=\"ajax('ExperimentName', ['project_name_" + tmpi + "', 'experiment_type_" + tmpi + "'], 'experiment_name_" + tmpi + "');\">"
    selected = "selected"
    for experimenttypelist in session.experimenttypelists:
        result += "<option value='" + experimenttypelist + "' " + selected + ">" + experimenttypelist + "</option>"
        selected = ""
    result += "</select>"
    result += "<span id='experiment_name_" + tmpi + "'></span>"
    return XML(result)

def ExperimentName():
    tmpi = session.tmpi
    exec('projectcodes = request.vars.project_name_' + tmpi)
    exec('experimenttypelists = request.vars.experiment_type_' + tmpi)
    #projectcodes = request.vars.project_name
    #experimenttypelists = request.vars.experiment_type
    try: projectcodes = projectcodes.split()
    except AttributeError: pass
    try: experimenttypelists = experimenttypelists.split()
    except AttributeError: pass
    if "None" in projectcodes and len(projectcodes) > 1:
        response.flash=T("\"None\" was also included in the multiselected project list")
    srvr = jsonrpclib.Server(openbisurl + '/openbis/rmi-general-information-v1.json')
    result = ""
    experimentcodes = []
    for experimenttypelist in experimenttypelists:
        experiments = srvr.listExperiments(session.st,session.projects,experimenttypelist)
        for experiment in experiments:
            experimentcodes.append(experiment['identifier'])
    result += "<br><label id='experiment_name" + tmpi + "' for='experiment_name_" + tmpi + "'>Experiment Name: </label>"
    result += "<select name='experiment_name_" + tmpi + "' id='experiment_name_" + tmpi + "' multiple onchange=\"ajax('SampleType' , ['experiment_name_" + tmpi + "'], 'sample_type_" + tmpi + "');\">"
    #result += "<select name='experiment_name_" + tmpi + "' id='experiment_name_" + tmpi + "' multiple onchange=\"ajax('SampleName' , ['experiment_name_" + tmpi + "'], 'sample_name_" + tmpi + "');\">"
    #result += "<select name='experiment_name_" + tmpi + "' id='experiment_name_" + tmpi + "' multiple onchange=\"\">"
    #result += "<option disabled selected value> -- select experiment -- </option>"
    for experimentcode in experimentcodes:
        for projectcode in projectcodes:
            if experimentcode.startswith(projectcode + '/'):
                result += "<option value='" + str(experimentcode) + "'>" + str(experimentcode) + "</option>"
    result += "</select>"
    result += "<span id='sample_type_" + tmpi + "'></span>"
    return XML(result)

def SamplePropertyType(): 
    tmpi = session.tmpi
    tmpj = str(int(session.tmpj) - 1)
    result = ""
    if not int(tmpj) == 0:
        exec('ptype = str(request.vars.sample_property_type_' + tmpi + '_' + str(int(tmpj) -1) + ")")
        exec('pval = str(request.vars.sample_property_value_' + tmpi + '_' +  str(int(tmpj) -1) + ")")
        exec('padd = str(request.vars.sample_property_value_add_' + tmpi + '_' +  str(int(tmpj) -1) + ")")
        tmpfiltercriteriavals = [ ptype, pval, padd ]
        exec('session.filtercriteriavals_' + tmpi + '.append(tmpfiltercriteriavals)')
        result += "<p>" + ptype + ": "
        if padd != 'varchar' and padd != 'timestamp' and padd != 'vocab':
            result +=  padd + " "
        result += pval + "</p>"
        result += "<style>p { border-left: 6px solid red; background-color: lightgrey;}</style>"
    sampletypes = session.sampletypes
    sampletypecode = session.sampletypecode
    exec("sample_type_" + tmpi + "= sampletypecode")
    result += "<select name='sample_property_type_" + tmpi + "_" + tmpj + "' id='sample_property_type_" + tmpi + "_" + tmpj + "' onchange=\"ajax('SamplePropertyFilter', ['sample_property_type_" + tmpi + "_" + tmpj + "'], 'sample_property_value_" + tmpi + "_" + tmpj + "')\">"
    result += "<option disabled selected value> -- select filter type -- </option>"
    for sampletype in sampletypes:
        if sampletype['code'] == sampletypecode:
            session.propertytypes = sampletype['propertyTypeGroups'][0]['propertyTypes']
            for propertytype in session.propertytypes:
                result += "<option value='" + str(propertytype['code']) + "'>" + str(propertytype['label']) + "</option>"
    #result += "<option value='x'>" + tmpj + "</option>"
    #result += "<option value='x'>" + tmpi + "</option>"
    result += "</select>"
    result += "<span id='sample_property_value_" + tmpi + "_" + tmpj + "'></span>"
    result += "<span id='span_button_filter_criteria_" + tmpi + "_" + str(int(tmpj) + 1) + "'></span>"
    result += "<script>"
    result += "$( document ).ready(function() {"
    result += " $.ajax({"
    result += "  type: \"GET\","
    result += "  url: \"FilterCriteriaVals\","
    result += "}).done(function( msg ) {$('#span_button_filter_criteria_" + tmpi + "_" + str(int(tmpj) + 1) + "').html( msg ) }); });"
    result += "</script>"
    return XML(result)

def SamplePropertyFilter():
    tmpi = session.tmpi
    tmpj = str(int(session.tmpj) - 2)
    propertytypes = session.propertytypes
    exec("samplepropertytype = request.vars.sample_property_type_" + tmpi + "_" + tmpj)
    sampletypecode = session.sampletypecode
    for propertytype in propertytypes:
        if propertytype['code'] == samplepropertytype:
            propertydatatype = propertytype['dataType']
            result = SamplePropertyDataType(sampletypecode, samplepropertytype, propertydatatype, tmpi, tmpj)
            break 
    #result += "<h1>" + str(sampletypecode) + "_" + str(samplepropertytype) + "_" + str(propertydatatype) + "_" + tmpi + tmpj  + "</h1>"
    return XML(result)

def SamplePropertyDataType(sampletypecode, samplepropertytype, propertydatatype, tmpi, tmpj):
    result = ""
    if propertydatatype == "INTEGER":
        result += "<input type='number' name='sample_property_value_" + tmpi + "_" + tmpj + "' id='sample_property_value_" + tmpi + "_" + tmpj + "' min='0'>"
        result += "<select name='sample_property_value_add_" + tmpi + "_" + tmpj + "'>"
        result += "<option value='='>=</option>"
        result += "<option value='<='><=</option>"
        result += "<option value='>='>>=</option>"
        result += "<option value='>'>></option>"
        result += "<option value='<'><</option>"
        result += "</select>"
    if propertydatatype == "VARCHAR":
        result += "<input type='text' name='sample_property_value_" + tmpi + "_"  + tmpj + "' id='sample_property_value_" + tmpi + "_" + tmpj + "'>"
        result += "<input type='hidden' name='sample_property_value_add_" + tmpi + "_" + tmpj + "' value='varchar'>"
    if propertydatatype == "TIMESTAMP":
        result += "<input type='text' name='sample_property_value_" + tmpi + "_"  + tmpj + "' id='sample_property_value_" + tmpi + "_" + tmpj + "'>"
        result += "<input type='hidden' name='sample_property_value_add_" + tmpi + "_" + tmpj + "' value='timestamp'>"
    if propertydatatype == "CONTROLLEDVOCABULARY":
        result += "<select name='sample_property_value_" + tmpi + "_" + tmpj + "' id='sample_property_value_" + tmpi + "_" + tmpj + "'>"
        selected = "selected"
        for propertytypelist in session.propertytypelists:
            if propertytypelist['code'] == samplepropertytype:
                samplepropertycontrolledvocab = propertytypelist['vocabulary']['code']
        for controlledvocab in session.controlledvocabs:
            if controlledvocab['code'] == samplepropertycontrolledvocab:
                for vocablist in controlledvocab['terms']:
                    result += "<option value='" + vocablist['code'] + "' " + selected + " >" + vocablist['label'] + "</option>"
                    selected = ""
        result += "</select>"
        result += "<input type='hidden' name='sample_property_value_add_" + tmpi + "_" + tmpj + "' value='vocab'>"
    return result

def SampleType():
    tmpi = session.tmpi
    gwinputanno = str(session.gwinputanno).split("+")
    gwinputanno = [x.upper().lower() for x in gwinputanno]
    exec("experiment_name_" + tmpi + " = request.vars.experiment_name_" + tmpi)
    result = "<label id='sample_type" + tmpi + "' for='sample_type_" + tmpi + "'>Sample Type: </label>"
    result += "<select name='sample_type_" + tmpi + "' id='sample_type_" + tmpi + "' onchange=\"ajax('SampleTableList', ['sample_type_" + tmpi + "', 'experiment_name_" + tmpi + "'], 'sample_table_list_" + tmpi + "');\">"
    #result += "<select name='sample_type_" + tmpi + "' id='sample_type_" + tmpi + "' onchange=\"ajax('SampleName', ['experiment_name_" + tmpi + "', 'sample_type_" + tmpi + "'], 'sample_name_" + tmpi + "');\">"
    selected = "selected"
    result += "<option disabled selected value> -- select sample type -- </option>"
    for sampletypelist in session.sampletypelists:
        if 'None'.upper().lower() in gwinputanno or sampletypelist.upper().lower() in gwinputanno:
            result += "<option value='" + sampletypelist + "'>" + sampletypelist + "</option>"
            selected = ""
    result += "</select>"
    result += "<br><span id='sample_table_list_" + tmpi + "'></span>"
#result += "<span id='sample_name_" + tmpi + "'></span>"
    return XML(result)

def SampleName():
    tmpi = session.tmpi
    exec("experimentcodes = request.vars.experiment_name_" + tmpi)
    exec("sampletypecode = request.vars.sample_type_" + tmpi)
    try: experimentcodes = experimentcodes.split()
    except AttributeError: pass
    result = "<label id='sample_name" + tmpi + "' for='sample_name_" + tmpi + "'>Sample Name: </label>"
    result += "<select name='sample_name_" + tmpi + "' id='sample_name_" + tmpi + "' multiple onchange=\"\">"
    selected = "selected"
    srvr = jsonrpclib.Server(openbisurl + '/openbis/rmi-general-information-v1.json')
    samplecodes = []
    for experimentcode in experimentcodes:
        samplelists = srvr.listSamplesForExperiment(session.st,experimentcode)
        for samplelist in samplelists:
            try:
                galaxy_api_id = samplelist['properties']['GALAXY_API_ID']
            except KeyError:
                session.flash=T('Sample not in sync with Galaxy')
                #redirect(URL('oops'))
            if samplelist['sampleTypeCode'] == sampletypecode:
                samplecodes.append([samplelist['permId'],samplelist['properties']['NAME'],samplelist['properties']])
    for samplecode in samplecodes:
        result += "<option value='" + str(samplecode) + "'>" + str(samplecode[1]) + "</option>"
    result += "</select>"
    return XML(result)

def SampleSelect(experimentcodes, sampletypecode):
    tmpi = session.tmpi
    try: experimentcodes = experimentcodes.split()
    except AttributeError: pass
    srvr = jsonrpclib.Server(openbisurl + '/openbis/rmi-general-information-v1.json')
    samplecodes = []
    samplecodesproperties = []
    for experimentcode in experimentcodes:
        try:
            samplelists = srvr.listSamplesForExperiment(session.st,experimentcode)
            for samplelist in samplelists:
                #try:
                #    galaxy_api_id = samplelist['properties']['GALAXY_API_ID']
                #except KeyError:
                #    session.flash=T('Sample not in sync with Galaxy')
                    #redirect(URL('oops'))
                if samplelist['sampleTypeCode'] == sampletypecode:
                    samplecodes.append([samplelist['permId'],samplelist['properties']['NAME'],samplelist['properties']])
                    samplecodesproperties += samplelist['properties'].keys()
            samplecodesproperties = list(set([str(tmpk) for tmpk in samplecodesproperties]))
            #samplecodesproperties = list(set(samplecodesproperties))
        except:
            samplecodes = []
            samplecodesproperties = []
    session.samplecodes = samplecodes
    session.samplecodesproperties = samplecodesproperties
    return

def SampleTableList():
    tmpi = session.tmpi
    result = ""
    exec("sampletypecode = request.vars.sample_type_" + tmpi)
    exec("experimentcodes = request.vars.experiment_name_" + tmpi)
    SampleSelect(experimentcodes, sampletypecode)
    samplecodes = session.samplecodes
    #result += "<h1>" + str(session.samplecodesproperties) + "</h1>"
    if len(samplecodes) > 0:
        #tmpjson = samplecodes[0][2]
        tmpstrs = session.samplecodesproperties
        columnnames = "<tr><th>permId</th>"
        ncolval = {}
        ncolnum = 0
        #for t in tmpjson.keys():
        for t in tmpstrs:
            ncolnum += 1 # permid is the first column thats y
            if t == 'GALAXY_API_ID':
                ncolval.update({ 'idcol' : ncolnum })
            elif t == 'NAME':
                ncolval.update({ 'namecol' : ncolnum})
            for propertytypelist in session.propertytypelists:
                if t == propertytypelist['code']:
                    if propertytypelist['dataType'] == "INTEGER":
                        columnnames += "<th class='char-filter'>" + str(t) + "</th>"
                    elif propertytypelist['dataType'] == "CONTROLLEDVOCABULARY":
                        columnnames += "<th class='vocab-filter'>" + str(t) + "</th>"
                    elif propertytypelist['dataType'] == "VARCHAR":
                        columnnames += "<th class='char-filter'>" + str(t) + "</th>"
                    elif propertytypelist['dataType'] == "TIMESTAMP":
                        columnnames += "<th class='char-filter'>" + str(t) + "</th>"
                    else:
                        columnnames += "<th>" + str(t) + "</th>"
        #result += "<h1>" + str(ncolval) + "</h1>"
        columnnames += "</tr>"
        jsvar_content = []
        for samplecode in samplecodes:
            #jsvar_content.append([str(samplecode[0])] + [str(tmpt) for tmpt in samplecode[2].values()])
            tmpcontent = [str(samplecode[0])]
            for t in tmpstrs:
                try: tmpcontent += [str(samplecode[2][t])]
                except: tmpcontent += [""] 
            jsvar_content.append(tmpcontent)
        #result += "<h1>" + str(session.propertytypelists) + "</h1>"
        #result += "<h1>" + str(jsvar_content) + "</h1>"
        result += "<table id='sample_table_lists_" + tmpi + "' class='display' width='100%'>"
        result += "<thead>" + columnnames + "</thead>"
        result += "<tfoot>" + columnnames + "</tfoot>"
        result += "</table>"
        result += "<button id='button_sample_table_reset_" + tmpi + "' name='button_sample_table_reset_" + tmpi + "' type='button'>Reset Filter</button>"
        result += "<button id='button_sample_table_lists_" + tmpi + "' name='button_sample_table_lists_" + tmpi + "' type='button'>Done</button>"
        result += "<input name='selected_sample_" + tmpi + "' id='selected_sample_" + tmpi + "' type='hidden' value=''/>"
        result += "<script>"
        result += "var dataSet = " + str(jsvar_content) + ";"
        result += "$(document).ready(function(){"
#
        result += "$('#sample_table_lists_" + tmpi + " tfoot th').each(function(){"
        result += "var title = $(this).text();"
        #result += "$(this).html('<input type=\"text\" placeholder=\"Search ' + title + '\" />');});"
        result += "$(this).html('<input class=\"charfilter\" type=\"text\" placeholder=\"Search ' + title + '\" />');});"
#
        result += "var table = $('#sample_table_lists_" + tmpi + "').DataTable({"
        result += "data: dataSet,"
        result += "columnDefs : [{ 'targets' : [ 0 ], 'visible': false, 'searachable': false }],"
        result += "scrollY: '200px', scrollX: '80%', 'scrollCollapse': true, 'paging': false,"
        result += "dom: 'Brtip',"
        result += "buttons: [{text: 'Select all', action: function(){table.rows({filter:'applied'}).select();}},{text: 'Select none', action: function(){table.rows().deselect();}}],"
        result += "initComplete: function(){ this.api().columns('.vocab-filter').every(function(d,j){"
        result += "var column = this; var select = $('<select class=\"vocabfilter\"><option value=\"\"></option></select>').appendTo("
        #result += "var column = this; var select = $('<select id=\"' + d + '\"><option value=\"\"></option></select>').appendTo("
        result += "$(column.footer()).empty()).on('change',function(){var val=$.fn.dataTable.util.escapeRegex("
        result += "$(this).val()); column.search(val ? '^'+val+'$' : '', true,false).draw();});"
        result += "column.data().unique().sort().each(function(d,j){ select.append('<option value=\"'+d+'\">'+d+'</option>')});"
        result += "});"
        result += "},"
        result += "});"
#
        result += "table.columns().eq(0).each(function(colidx){"
        result += "$('input',table.column(colidx).footer()).on('keyup change', function(){"
        result += "table.column(colidx).search(this.value).draw();});});"
#
        result += "$('#sample_table_lists_" + tmpi + " tbody').on('click','tr', function(){$(this).toggleClass('selected');});"
        result += "$('#button_sample_table_reset_" + tmpi + "').click(function(){"
        result += "table.search('').draw(); table.columns().search('').draw();"
        result += "$('input[class=charfilter]').each(function(){$(this).val('');});"
        result += "$('select[class=vocabfilter]').each(function(){$(this).val('');});"
        result += "});"       
        #result += "$('#button_sample_table_lists_" + tmpi + "').click(function(){alert(table.rows('.selected').data().length);});"
        result += "$('#button_sample_table_lists_" + tmpi + "').click(function(){"
        result += "var data = table.rows('.selected').data();"
        result += "var selected_samples = [];"
        result += "var tmpidvar;"
        result += "for(var i=0;i<data.length;i++){ selected_samples.push(data[i]["
        result += str(ncolval['idcol'])
        result += "].split('_')[1] + '#$#' + data[i]["
        result += str(ncolval['namecol'])
        result += "]);};"
        #result += "var selected_samples = '[' + selected_samples + ']';"
        result += "var selected_samples = 'openbis,' + selected_samples;"
        result += "alert(selected_samples);"
        result += "document.getElementById('selected_sample_" + tmpi + "').value = selected_samples;"
        result += "});"
#
#
        result += "});"
        result += "</script>"
    else:
        #session.flash=T('No samples for the selected sample type')
        result += "<script>"
        result += "$( document ).ready(function() {"
        result += " $.ajax({"
        result += "  type: \"GET\","
        result += "  url: \"SampleType\","
        result += "}).done(function( msg ) {$('#sample_type_" + tmpi + "').html( msg ) });});"
        result += "</script>"
    return XML(result)

def WorkflowInputs(gworkflowid):
    gwdict = session.gi.workflows.show_workflow(gworkflowid)
    gwinputs = []
    for w in gwdict['inputs']:
        gwinputs.append([w, gwdict['inputs'][w]['label']])
    for gwinput in gwinputs:
        gwinput.append(gwdict['steps'][gwinput[0]]['annotation'])
    session.gwname = gwdict['name']
    session.gwinputs = gwinputs
    return

def sync_openbis_galaxy():
    session.dlibid = session.dlibfolder = None
    gdatatypes = session.gi.datatypes.get_datatypes()
    gdatatypes = [str(d) for d in gdatatypes]
    odatatypes = session.sampletypelists
    gdatatypes_mod = []
    for gdatatype in gdatatypes:
        if gdatatype.upper() in odatatypes:
            gdatatypes_mod.append(gdatatype)
    #redirect(URL(openbis_upload_form, vars=dict(gdatatype=session.gdatatype)))
    return dict(gdatatypes=gdatatypes_mod)

def galaxy_dlib_folders():
    result = ""
    session.dlibid = request.vars.dlibid
    rr = requests.get(session.gi.libraries.url + "/" + session.dlibid + "/contents", params= session.gi.default_params, verify=False)
    if rr.status_code != 200:
        session.flash=T('Galaxy Data Library exit with error code')
        redirect(URL('oops'))
    dliblist = json.loads(rr.content)
    result += "<br><label for='galaxy_dlibfol'>Select root folder: </label>"
    result += "<select name='dlibfolder' id='dlibfolder' onchange=\"ajax('galaxy_dlib_folder_session', ['dlibfolder'], '');\">"
    result += "<option disabled selected value> -- select galaxy library root folder -- </option>"
    for d in dliblist:
        if d['type'] == 'folder':
            result += "<option value='" + d['id'] + "' >" + d['name'] + "</option>"
    result += "</select>"
    return XML(result)

def galaxy_dlib_folder_session():
    session.dlibfolder = request.vars.dlibfolder
    return dict()

def sync_option_session():
    session.syncoption = request.vars.syncoption
    return dict()

def upload_sample_status_session():
    session.samplestatus = request.vars.samplestatus
    return dict()

def upload1_option_session():
    session.upload1_option_session = request.vars.upload1_opt
    result = ""
    if session.upload1_option_session == "fromform":
        tmpfile = request.folder + 'static/openbis_templates/openbis_' + session.gdatatype + '_template.tsv'
        proc = subprocess.Popen('grep -v ^# %s | grep -v ^\\"# | head -1' %tmpfile, shell=True, stdout=subprocess.PIPE)
        ( out,err ) = proc.communicate()
        result += "<br>"
        for i in range(0,len(out.split("\t"))):
            result += out.split("\t")[i] + "  <input type='text' id='upload1_" + str(i) + "' value=None><br>"
        result += "<button id='button_upload1' name='button_upload1' type='button'>Done</button>"
        result += "<span id='spantmp'></span>"
        result += "<script>$(function(){"
        result += "$('#button_upload1').click(function(){"
        result += "$.ajax({ type : \"POST\", url: \"upload1_fromform\", data: {"
        for i in range(0,len(out.split("\t"))):
            #result += "'upload1_" + str(i) + "' : $('#upload1_" + str(i) + "').val() "
            result += "'upload1_" + str(i) + "' : document.getElementById('upload1_" + str(i) + "').value "
            if i < (len(out.split("\t")) - 1):
                result += ","
        result += "}}).done(function(msg){$('#spantmp').html(msg)}); });});</script>"
        session.tmpleteout = out
    return XML(result)

def upload1_fromform():
    out = session.tmpleteout
    for i in range(0,len(out.split("\t"))):
        if i == 0:
            exec('templatestr = str(request.vars.upload1_' + str(i) + ")")
        else:
            exec('templatestr = templatestr + \'\t\' + str(request.vars.upload1_' + str(i) + ")")
    result = templatestr
    sample_update = openbis_upload1_fromform_run()
    result += str(len(templatestr.split("\t")))
    stringtowrite = ""
    outnew = out.split("\t")
    templatestr = templatestr.split("\t")
    for f in sample_update:
        for v in range(0,len(outnew)):
            if outnew[v] == "NAME":
                stringtowrite = stringtowrite + "\t" + f
            elif outnew[v] == "GALAXY_API_ID":
                stringtowrite = stringtowrite + "\t" + sample_update[f]
            else:
                if templatestr[v] != "None":
                    if v == 0:
                        stringtowrite = stringtowrite + templatestr[v]
                    else:
                        stringtowrite = stringtowrite + "\t" + templatestr[v]
                else:
                    if v == 0:
                        stringtowrite = stringtowrite + ""
                    else:
                        stringtowrite = stringtowrite + "\t" + ""
        stringtowrite = stringtowrite + "\n"
    tmpfilewrite = request.folder + "uploads/" + str(session.gemail.replace("@","_")) + "_" + str(int(round(time.time() * 1000))) + "_dynamic.tsv"
    tmpfile2 = open(tmpfilewrite, 'w')
    tmpfile2.write(out)
    tmpfile2.write(stringtowrite)
    tmpfile2.close()
    os.system('dos2unix ' + tmpfilewrite)
    redirect(URL('openbis_upload_run', vars=dict(ufilename = tmpfilewrite)))
    return XML(result)

def openbis_upload1_fromform_run():
    lib_id = session.dlibid
    fol_id = session.dlibfolder
    rr = requests.get(session.gi.folders.url + "/" + fol_id + "/contents" , params=session.gi.default_params, verify=False)
    if rr.status_code != 200:
        session.flash=T('Galaxy Data library Folder returned with error code')
        redirect(URL('oops'))
    folder_list = json.loads(rr.content)['folder_contents']
    sample_update = {}
    for d in folder_list:
        if d['type'] == 'file':
            tmpd = session.gi.libraries.show_dataset(lib_id, d['id'])
            if tmpd['file_ext'] == session.gdatatype:
                sample_update[tmpd['name']] = str(lib_id) + "_" + str(tmpd['id'])
    return sample_update


def openbis_apicalls():
    result = ""
    session.gdatatype = request.vars.gdatatype
    gdatatype= session.gdatatype
    calloption = request.vars.calloption
    if calloption == "template":
        tmpfile = request.folder + 'static/openbis_templates/openbis_' + session.gdatatype + '_template.tsv'
        if not os.path.exists(tmpfile):
            #openbis_api_template = javapath + ' -jar ' + openbisapijar + ' getsampletemplate -k ' + openbiskeystore + ' -r ' + openbisurl + ' -t ' + session.st + ' -s ' + session.gdatatype.upper() + ' -o ' + tmpfile
            openbis_api_template = javapath + ' -jar ' + openbisapijar + ' getsampletemplate ' + openbiskeystore + ' -r ' + openbisurl + ' -t ' + session.st + ' -s ' + session.gdatatype.upper() + ' -o ' + tmpfile
            proc = subprocess.Popen(openbis_api_template, shell=True, stdout=subprocess.PIPE)
            ( out,err ) = proc.communicate()
            if err:
                session.flash=T('Error processing uploaded file')
                redirect(URL('oops'))
    result += "<br><br>"
    result += "<label for='galaxy_dlib'>Select Data Library: </label>"
    result += "<select name='dlib' id='dlib'>"
    result += "<option disabled selected value> -- select galaxy data library -- </option>"
    gdatalib = session.gi.libraries.get_libraries()
    for dlib in gdatalib:
        if not dlib['deleted']:
            result += "<option value='" + dlib['id'] + "'>" + dlib['name'] + "</option>"
    result += "</select>"
    result += "<span id='span_dlib'/>"
    result += "<script type=\"text/javascript\">"
    result += "$(function(){"
    result += " $('#dlib').change(function(){"
    result += " $.ajax({ type: \"POST\", url: \"galaxy_dlib_folders\", data : {'dlibid' : $('#dlib :selected').val(), 'gdatatype' : $('#gdatatype :selected').val() }}).done(function( msg ) {$('#span_dlib').html( msg ) });});"
    result += "});"
    result += "</script>"
    return XML(result)

def openbis_upload_form():
    gdatatype = request.vars.gdatatype
    form = SQLFORM(db.openbis_uploads1, deletable=True, upload=URL('download'))
    form1 = form
    if form.process().accepted:
        response.flash = 'form accepted'
        ufilename = form.vars.Openbis_Data_Upload
        ufilename1 = form1.vars.Openbis_Data_Upload
        redirect(URL('openbis_upload_run', vars=dict(ufilename = ufilename)))
    elif form.errors:
        response.flash = 'form has errors'
        redirect(URL('oops'))
    return dict(form=form, gdatatype=gdatatype, form1=form1, tmp=str(session.dlibid) + str(session.dlibfolder))

def openbis_upload_run():
    session.sample_notupdated = []
    if session.upload1_option_session == "fromform":
        ufilename = request.vars.ufilename
        #if session.syncoption == 'newdata':
        #    openbis_api_newdata = javapath + ' -jar ' + openbisapijar + ' registersamples -k ' + openbiskeystore + ' -r ' + openbisurl + ' -t ' + session.st + ' -s ' + session.gdatatype.upper() + ' -i ' + ufilename
        #    #run java
        #    proc = subprocess.Popen(openbis_api_newdata, shell=True, stdout=subprocess.PIPE)
        #    ( out,err ) = proc.communicate()
        #    if err:
        #        session.flash=T('ERROR')
        #        redirect(URL('oops'))
        #    else:
        #        redirect(URL('finito'))
    else:
        ufilename = request.folder + "uploads/" + request.vars.ufilename
    if request.vars.ufilename1:
        ufilename1 = request.folder + "uploads/" + request.vars.ufilename1
    proc = subprocess.Popen('grep -v ^# %s | grep -v ^\\"# | wc -l' %ufilename, shell=True, stdout=subprocess.PIPE)
    ( out,err ) = proc.communicate()
    if int(out.rstrip('\n')) <= 1:
        session.flash=T("Input is empty")
        redirect(URL('oops'))
    else:
        tmpfile = ufilename + ".tmp"
        os.system('dos2unix ' + ufilename)
        proc = subprocess.Popen('grep -v ^# %s | grep -v ^\\"# > %s' %(ufilename,tmpfile), shell=True, stdout=subprocess.PIPE)
        ( out,err ) = proc.communicate()
        if err:
            session.flash=T("Error in uploaded file")
            redirect(URL('oops'))
        proc = read_upload_file_function(tmpfile)
        if session.syncoption == 'newdata':
            if session.identifiercol:
                session.flash=T('identifier column is not accepted for new samples')
                redirect(URL('oops'))
            if not session.dlibid or not session.dlibfolder:
                session.flash=T('Please Select valid datalibrary and library folder')
                redirect(URL('oops'))
            sample_update = galaxy_get_api_ids(tmpfile)
            #run openbis api
            #openbis_api_newdata = javapath + ' -jar ' + openbisapijar + ' registersamples -k ' + openbiskeystore + ' -r ' + openbisurl + ' -t ' + session.st + ' -s ' + session.gdatatype.upper() + ' -i ' + session.modfile
            openbis_api_newdata = javapath + ' -jar ' + openbisapijar + ' registersamples ' + openbiskeystore + ' -r ' + openbisurl + ' -t ' + session.st + ' -s ' + session.gdatatype.upper() + ' -i ' + session.modfile
            #run java
            proc = subprocess.Popen(openbis_api_newdata, shell=True, stdout=subprocess.PIPE)
            ( out,err ) = proc.communicate()
        elif session.syncoption == 'updateopenbis':
            err = "True"
            pass
        elif session.syncoption == 'updategalaxy':
            if not session.dlibid or not session.dlibfolder:
                session.flash=T('Please Select valid datalibrary and library folder')
                redirect(URL('oops'))
            err = "True"
            pass
        elif session.syncoption == 'linkmetaids':
            if not session.dlibid or not session.dlibfolder:
                session.flash=T('Please Select valid datalibrary and library folder')
                redirect(URL('oops'))
            if not request.vars.ufilename1:
                session.flash=T('Please Select a valid link file')
                redirect(URL('oops'))
            err = "True"
            pass
        if err:
            if err == "True":
                session.flash=T('Under Construction')
            elif err:
                session.flash=T('Error processing uploaded file')
            redirect(URL('oops'))
        else:
            session.flash=T(str(session.sample_notupdated))
            redirect(URL('finito_update', vars=dict(tmpfile=tmpfile)))
    return dict()

def read_upload_file_function(tmpfile):
    session.samplenamecol = session.galaxyidcol = session.identifiercol = session.metaidcol = session.sname = None
    tmpfile1 = open(tmpfile, 'r')
    tmpfile1 = tmpfile1.read()
    tmpint = 0
    sname = []
    for l in tmpfile1.strip('\n').split('\n'):
        if tmpint == 0:
            try:
                session.samplenamecol = l.split('\t').index('NAME')
            except ValueError:
                session.flash=T('NAME column not found in the uploaded file') 
                redirect(URL('oops'))
            try: session.galaxyidcol = l.split('\t').index('GALAXY_API_ID')
            except ValueError: pass
            try: session.identifiercol = l.split('\t').index('identifier')
            except ValueError: pass
            try: session.metaidcol = l.split('\t').index('METAID')
            except ValueError: pass
            tmpint = 1
        else:
            sname.append(l.split('\t')[session.samplenamecol])
    session.sname = sname
    return dict()

def galaxy_get_api_ids(tmpfile):
    lib_id = session.dlibid
    fol_id = session.dlibfolder
    rr = requests.get(session.gi.folders.url + "/" + fol_id + "/contents" , params=session.gi.default_params, verify=False)
    if rr.status_code != 200:
        session.flash=T('Galaxy Data library Folder returned with error code')
        redirect(URL('oops'))
    folder_list = json.loads(rr.content)['folder_contents']
    sname = session.sname
    samplenamecol = session.samplenamecol
    galaxyidcol = session.galaxyidcol
    identifiercol = session.identifiercol
    sample_update = {}
    for d in folder_list:
        if d['type'] == 'file':
            tmpd = session.gi.libraries.show_dataset(lib_id, d['id'])
            if tmpd['name'] in sname:
                if tmpd['file_ext'] == session.gdatatype:
                    sample_update[tmpd['name']] = str(lib_id) + "_" + str(tmpd['id'])
    if not sample_update:
        session.flash=T('Uploaded file doesn\'t have specified galaxy datatype')
        redirect(URL('oops'))
    elif len(sample_update) < len(sname):
        session.sample_notupdated = list(set(sname) - set(sample_update.keys()))
    session.modfile = tmpfile + ".mod.tsv"
    os.system('head -1 %s > %s' %(tmpfile, session.modfile))
    for snamevar in sample_update:
        proc = subprocess.Popen('grep -w "%s" %s | awk -v gid="%s" -v gval="%s" \'BEGIN{FS="\t"; OFS=FS;}{ for(i=1;i<=NF;i++) {if(i==1) printf $i; else if(i==gid) printf "\t"gval; else printf "\t"$i;} printf "\\n"}\' >> %s' %( snamevar, tmpfile, galaxyidcol+1, sample_update[snamevar], session.modfile),  shell=True, stdout=subprocess.PIPE)
        ( out,err ) = proc.communicate()
        if err:
            session.flash=T('Error processing uploaded file')
            redirect(URL('oops'))
    return sample_update
 
def finito_update():
    tmpfile = request.vars.tmpfile
    reqfile = "openbis_upload_failures/" + str(session.gemail.replace("@", "_")) + "_" + str(int(round(time.time() * 1000))) + "_failed_updates.tsv"
    tmpfile_notupdated = request.folder + "/static/" + reqfile
    if len(session.sample_notupdated) == 0:
        redirect(URL('finito'))
    else:
        os.system('head -1 %s > %s' %(tmpfile, tmpfile_notupdated))
        for snamevar in session.sample_notupdated:
            proc = subprocess.Popen('grep -w "%s" %s >> %s' %(snamevar, tmpfile, tmpfile_notupdated),  shell=True, stdout=subprocess.PIPE)
            ( out,err ) = proc.communicate()
            if err:
                session.flash=T('Error processing uploaded file')
                redirect(URL('oops'))
    return dict(reqfile=reqfile)    
    











