###
db.define_table('galaxy_openbis_connector1',
                Field('gapikey',
                      unique=True,
                      type='string',
                      label=T('Galaxy API Key')),
                Field('ousername',
                      unique=True,
                      type='string',
                      label=T('Openbis User Name')),
                Field('opassphrase',
                      type='string',
                      label=T('Openbis Password')),
                Field('guseremail',
                      unique=True,
                      type='string',
                      label=T('Galaxy User Email'))
                ) 

db.define_table('openbis_uploads1',
                Field('Openbis_Data_Upload', 'upload'))
